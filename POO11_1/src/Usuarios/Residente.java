/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Usuarios;

import Bienes.Casa;
import Bienes.Ciudadela;
import Bienes.Vehiculo;
import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author GaryBarzola
 */
public class Residente extends Usuario{
    private String phone, pin, cedula;
    private Casa casa;
    private ArrayList<Visitante> visitantes= new ArrayList<>();
    private ArrayList<Vehiculo> vehiculos= new ArrayList<>();
    
    
    public Residente(){
        //visitantes = new ArrayList<>();
        //vehiculos= new ArrayList<>();
        
    }   

    
    public Residente(String us, String ps, String nombre,String cedula, String correo, String phone , String pin, Casa casa){
        super(us, ps, nombre, correo);
        this.phone=phone;
        this.casa= casa;
        this.cedula= cedula;
        this.pin= pin; 
        
    }

    
    public void agregarVisitante(Visitante v){  //Me agrega visitantes al ArrayList respectivo
        visitantes.add(v);
    }

    
    public void agregarVehiculo(Vehiculo v){ // Me agrega vehiculos al ArrayList del residente correspondiente 
        vehiculos.add(v);
    }
    
    
    public void recorrerVehiculos(){  //  Este metodo sera eliminado para la presentacion
        for(int x=0;x<vehiculos.size();x++) {
            System.out.println(vehiculos.get(x));
        }
    }
    
    
    public void recorrerVisitantes(){  // Me muestra los visitantes que estan en el ArrayList
        for(int x=0;x<visitantes.size();x++) {
            System.out.println(visitantes.get(x));
        }
    }

    
    public String generarCodigo(){  //Genera Codigo de 8 digitos para visitantes
        String codigo="";
        for (int i=0; i<8 ; i++){
            Random n= new Random();
            int numero = n.nextInt(9);
            codigo+=Integer.toString(numero);
        }
        return codigo;
    }
    
    
    public String obtenerCorreoVisitante(String cedula){  //Metodo que me retorna el correo de un visitante en el caso de existir 
        for(Visitante v: visitantes){
            if(v.getCedula().equals(cedula)){
                return v.getCorreo();
            }
        }return null;
    }
    
    
    public boolean eliminarVisitante(String cedula){  //Elimina un visitante pasado por parametro su cedula
        for(int i=0;i<visitantes.size();i++){
            if(visitantes.get(i).getCedula().equals(cedula)){
                visitantes.remove(i);
                return true;
            }
            
        }return false; 
    }
    
    
    public String getCedula() {
        return cedula;
    }
    
    @Override
    public String toString() {
        return super.toString()+ ", Phone: " + phone + ", Pin: " + pin + ", cedula=" + cedula + ", casa: " + casa ;
    }
        
    
    public String getPhone() {
        return phone;
    }

    
    public Casa getCasa() {
        return casa;
    }
    
    
    public String getNombre(){
        return super.getNombre();
    }
    
    
    public String getCorreo(){
        return super.getCorreo();
    }
    
    
    public void setPin(String pin) {
        this.pin = pin;
    }

    
    public String getPin() {
        return pin;
    }

    public ArrayList<Visitante> getVisitantes() {
        return visitantes;
    }

    public ArrayList<Vehiculo> getVehiculos() {
        return vehiculos;
    }
     
    
       
}
    
    

