  /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Usuarios;

import java.util.Date;
/**
 *
 * @author Andrea
 */
public class AdminCiudadela extends Usuario {
    private String cedula;
    private Date fechaInicioCargo;
    private Date fechaFinCargo;
    
    
    
    public AdminCiudadela(){
        //Casa cas =new Casa();
        //cas = new Casa("calle la chala" , "92A");
        //residentes.add(new Residente("sergio" , "serleram@espol.edu.ec" , "0960546707" , cas, "0953487501" , "2016" ));
    }
    
    
    public AdminCiudadela(String us, String ps, String nombre, String correo, String cedula, Date fechaI, Date fechaF){
        super(us, ps, nombre, correo);
        this.cedula=cedula;
        this.fechaFinCargo=fechaI;
        this.fechaInicioCargo=fechaF;
    }

    
    public String getCedula(){
        return this.cedula;
    }

    
    public Date getFechaInicioCargo() {
        return fechaInicioCargo;
    }

    
    public Date getFechaFinCargo() {
        return fechaFinCargo;
    }

    
    public String getNombre(){
        return super.getNombre();
    }
    
    
    public String getCorreo(){
        return super.getCorreo();
    }   

    @Override
    public String toString() {
        return super.toString()+", Cedula: "+cedula+", Fecha Incio: "+fechaInicioCargo+", Fecha Fin: "+fechaFinCargo;
    }

    
    
    
       
}
