/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Usuarios;

import java.util.Date;

/**
 *
 * @author GaryBarzola
 */
public class Visitante extends Usuario {
    private String cedula, pin;
    private Date fechaIngreso;
    private Date fechaGeneracion;
    private boolean estado; //Si es true quiere decir que tiene permitido el ingreso a la ciudadela
                            //Si es false quiere decir que ya ingreso a la ciudadela.
    
    public Visitante(){
        
    }
    
    
    public Visitante(String nombre, String correo, String cedula, String pin, Date fechaIngreso, Date fechaGeneracion, boolean estado){
        super(nombre, correo);
        this.cedula=cedula;
        this.pin=pin;
        this.fechaIngreso=fechaIngreso;
        this.fechaGeneracion=fechaGeneracion;
        this.estado=estado;
    }

    
    public String getNombre(){
        return super.getNombre();
    }
    
    
    public String getCedula() {
        return cedula;
    }

    
    public String getPin() {
        return pin;
    }

    
    public void setPin(String pin) {
        this.pin = pin;
    }
    
    
    public Date getFechaIngreso() {
        return fechaIngreso;
    }

    
    public Date getFechaGeneracion() {
        return fechaGeneracion;
    }
    
    public boolean getEstado(){
        return this.estado;
    }
    
    public void setEstado(boolean estado){
        this.estado=estado;
    }

    @Override
    public String toString() {
        return "NOMBRE: "+ super.getNombre() + " CORREO:  "+ super.getCorreo()+ "  CEDULA: " + cedula + ", CODIGO DE ACCESO: " + pin + ", FECHA INGRESO: " + fechaIngreso + ", FECHA DE GENERACION: " + fechaGeneracion+", Estado: "+estado;
    }
    
    
    
  
}
