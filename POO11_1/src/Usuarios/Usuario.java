/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Usuarios;

import java.util.Date;

/**
 *
 * @author GaryBarzola
 */
public class Usuario {
    private String nombre, correo;
    private String usuario, contrasena;

    public Usuario(){
    }
    
    
    public Usuario(String usuario, String contrasena, String nombre, String correo) {
        this.nombre = nombre;
        this.correo = correo;
        this.contrasena=contrasena;
        this.usuario= usuario;
     
    }
    
    
    public Usuario(String nombre, String correo) {
        this.nombre = nombre;
        this.correo = correo;
    }
    
    
    public String getNombre() {
        return nombre;
    }

    public String getCorreo() {
        return correo;
    }

    public String getUsuario() {
        return usuario;
    }

    public String getContrasena() {
        return contrasena;
    }

    
    public String toString() {
        return "Usuario: "+usuario+", Contrasena: "+contrasena+", Nombre: "+ nombre + ", Correo: "+ correo;
    }
    
}
