/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bienes;

/**
 *
 * @author GaryBarzola
 */
public class Instalaciones {
    private String codigo, nombre;
    
    public Instalaciones(String c, String n){
        codigo=c;
        nombre=n;
    }
    
    public String getCodigo(){
        return codigo;
    }
    
    public String getNombre(){
        return nombre;
    }
    
    public String toString(){
        return codigo+","+nombre;
    }
    
    
}
