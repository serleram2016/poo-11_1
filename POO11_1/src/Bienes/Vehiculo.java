/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bienes;


/**
 *
 * @author GaryBarzola
 */
public class Vehiculo {
    private String nMatricula;
    private String propietario;

    public Vehiculo(String nMatricula, String propietario) {
        this.nMatricula = nMatricula;
        this.propietario = propietario;
    }
    

    public String getnMatricula() {
        return nMatricula;
    }

    public String getPropietario() {
        return propietario;
    }

    
    public String toString() {
        return  "nMatricula=" + nMatricula + ", propietario=" + propietario;
    }
    
    
    
    
    
}
