/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bienes;

import Reportes.ReporteVisitas;
import Usuarios.AdminCiudadela;
import Usuarios.Residente;
import Usuarios.Visitante;
import java.util.ArrayList;

/**
 *
 * @author GaryBarzola
 */
public class Ciudadela {
    private String nombre, razonSocial, ruc, ubicacion;
    private AdminCiudadela adminCiudadela; // Administrador de la ciudadela
    private ArrayList<Casa> casas = new ArrayList<>(); //
    private ArrayList<Residente> residentes= new ArrayList<>(); // Residentes de la ciudadela
    
    private ArrayList<ReporteVisitas> listaVisitas=new ArrayList<>();  //Usuarios que ingresan a la ciudadela
    
    private int numVisitantes=0;
    private int numResidentes=0;
    
    private double tiempo_Visitante=0;
    private double tiempo_Residente=0;
    
    public Ciudadela(){
    }

    
    public Ciudadela(String nombre, String razonSocial, String ruc, String ubicacion, AdminCiudadela adminCiudadela) {
        this.nombre = nombre;
        this.razonSocial = razonSocial;
        this.ruc = ruc;
        this.ubicacion = ubicacion;
        this.adminCiudadela=adminCiudadela;   
    }
      
    
    public void  agregarResidente(Residente r){  //Me agrega Objetos de tipo residente al ArrayList respectivo
        residentes.add(r);
    }
    
    
    public void agregarCasa(Casa c){  //Me agrega Objetos de tipo casa al ArrayList respectivo
        casas.add(c);
    }
    
    
    public void agregarListaReporteVisitas(ReporteVisitas s){  //Llena el ArrayList ReporteVisitas 
        listaVisitas.add(s);
    }
    
    
    public void contarVisitante(){ //Si un visitante ingresa a la ciudadela se lo cuenta
        numVisitantes++;
    }
    
    
    public void contarResidente(){ //Si un residente ingresa a la ciudadela se lo cuenta
        numResidentes++;
    }
    
    public void registrarTiempoVisitante(double tiempo){ //Guarda el tiempo que le tomo a un visitante ingresar a la ciudadela
        tiempo_Visitante+=tiempo;
    }
    
    public void registrarTiempoResidente(double tiempo){  //Guarda el tiempo que le tomo a un residente ingresar a la ciudadela
        tiempo_Residente+=tiempo;
    }
    
        
    public Casa buscarCasa(String mz, String villa){ //Me retorna una Casa pasando por parametro su Mz y Villa
        for(Casa c: casas){
            if(c.getMz().equals(mz) && c.getVilla().equals(villa)){
                return c;
            }
        }return null; 
    }
    
    
    public boolean ingresoPeatonResidente(String cedula, String pin){ //Metodo que verifica si un residente existe ingreso a la ciudadela.
        for (Residente resident: residentes){
            if ( resident.getCedula().equals(cedula) && resident.getPin().equals(pin) && pin.length()==4){
                System.out.println("..... BIENVENIDO SR. " + resident.getNombre()+ " Buenas tardes.........");
                return true;}
        }
        System.out.println("ACCESO DENEGADO...........\n");
        return false;
    }
    
    
    public boolean validarResidentePV(Residente re){  //Valida si un residente esta en el ArrayList solo con el nombre y la casa
        for (Residente r : residentes){
            Casa c1= re.getCasa();
            Casa c2= r.getCasa();
            if (r.getNombre().equals(re.getNombre()) && c1.getMz().equals(c2.getMz()) &&
                    c1.getVilla().equals(c2.getVilla()) ) {
                return true;
            }
        }return false;
    }
    
    
    public void recorreArregloR(){  //Recorre el ArrayList residentes para hacer pruebas
        for(int i=0; i<residentes.size();i++){
            System.out.println(residentes.get(i)) ;
        }
    }
   
    
    public Residente recorrerResidente( String cedula){ // este metodo recorre los residentes y busca con la cedula
        for ( Residente rv: residentes){
            if (rv.getCedula().equals(cedula)){
                return rv;
            }
        }
        return null;
    }
    
    
    public boolean buscarVehiculo(String matricula){
        for(Residente r: residentes){
            for(Vehiculo v: r.getVehiculos()){
                if(v.getnMatricula().equals(matricula)){
                    return true;
                }
            }
        }return false;
    }
    
    public boolean validarCodigo(String codigo){  //Valida que un codigo de visitante exista
        for(Residente r: residentes){
            for(Visitante v: r.getVisitantes()){
                if(v.getPin().equals(codigo))
                    return true;
            }    
        }return false;      
    }
    
    
    public Residente obtenerResidente(String cedula){
        for(Residente r: residentes){
            if(r.getCedula().equals(cedula))
                return r;
        }return null;
    }
    
    
    public Residente buscarResidenteParaVisitante(String nombre, String mz, String villa){  //Metodo que me busca un residente para el visitante que no tiene codigo de acceso y que esta en la simulacion. 
        for(Residente r: residentes){
            Casa c=r.getCasa();
            if(r.getNombre().equalsIgnoreCase(nombre) && c.getMz().equals(mz) && c.getVilla().equals(villa))
               return r;   
        }return null;
    }
    
    
    public String getNombre() {
        return nombre;
    }

    
    public String getRazonSocial() {
        return razonSocial;
    }

    
    public String getRuc() {
        return ruc;
    }

    
    public AdminCiudadela getAdminCiudadela() {
        return adminCiudadela;
    }

    
    public String getUbicacion() {
        return ubicacion;
    }

    
    public ArrayList<Casa> getCasas() {
        return casas;
    }

    
    public ArrayList<Residente> getResidentes() {
        return residentes;
    }
    
    
    public ArrayList<ReporteVisitas> getListaVisitas() {
        return listaVisitas;
    }

    public int getNumVisitante() {
        return numVisitantes;
    }
    
    
    public int getNumResidente() {
        return numResidentes;
    }

    
    public double getTiempo_Visitante() {
        return tiempo_Visitante;
    }

    
    public double getTiempo_Residente() {
        return tiempo_Residente;
    }

    
    public String toString() {
        return "Nombre: " + nombre + ", RazonSocial: " + razonSocial + ", Ruc: " + ruc + ", Ubicacion: " + ubicacion+", Administrador: "+adminCiudadela ;
    }
    
    
}
