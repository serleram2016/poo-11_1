/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bienes;

/**
 *
 * @author GaryBarzola
 */
public class Casa {
    private String villa;
    private String mz;
    private boolean disponibilidad;
    
    public Casa(String mz, String villa, boolean disponibilidad) {
        this.villa = villa;
        this.mz = mz;
        this.disponibilidad=disponibilidad;
    }

    
    public boolean getDisponibilidad() {
        return disponibilidad;
    }

    
    public void setDisponibilidad(boolean disponibilidad) {
        this.disponibilidad = disponibilidad;
    }

    
    public String getVilla() {
        return villa;
    }

    
    public String getMz() {
        return mz;
    }

    
    public String toString() {
        return "Mz: " + mz.substring(2, mz.length()) + ", villa: " + villa.substring(5, villa.length())+", Disponibilidad: "+disponibilidad;
    }
    
    
    
    
}
