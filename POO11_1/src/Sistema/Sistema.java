/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Sistema;

import Bienes.*;
import Correo.Correo;
import Reportes.*;
import Usuarios.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author GaryBarzola
 */
public class Sistema {
    private static ArrayList<Ciudadela> ciudadelas;
    private static ArrayList<Usuario> usuarios;
    private static ArrayList<ReporteDeUsos> reporteUsos;
    

    public Sistema(){
        ciudadelas = new ArrayList<>();
        usuarios = new ArrayList<>();
        //listaVisitas= new ArrayList<>();
        reporteUsos= new ArrayList<>();
        usuarios.add(new AdminSistema("superadmin","superadmin",null,null));
       
    }
    

    public static void llenarListaCiudadelas(Ciudadela c){ //Llena el ArrayList Ciudadelas
        ciudadelas.add(c);
    }
    
    
    public static void llenarListaUsuarios(Usuario u){  //Llena el ArrayList Usuarios
        usuarios.add(u);
    }
    
    
    public static void llenarListaReporteUsos(ReporteDeUsos s){  //Llena el ArrayList ReporteUsos 
        reporteUsos.add(s);
    }

    
    public Usuario autenticacionUsuario(String user, String pass){ //Me valida el usuario y me lo retorna
        for(Usuario u: usuarios){
            if(u.getUsuario().equals(user) && u.getContrasena().equals(pass)){
                return u;
            }
        }return null;
    }

    
    public static  void crearUsuario(Usuario user){  //Crea un usuario y se lo manda al correo
        String usuario;
        String contrasena;
        if(user instanceof AdminCiudadela){
            AdminCiudadela admC= (AdminCiudadela)user;
            usuario=admC.getCedula();
            contrasena=admC.getNombre().substring(0, 3)+"adm";
            usuarios.add(new AdminCiudadela(usuario, contrasena, admC.getNombre(), admC.getCorreo(), admC.getCedula() , admC.getFechaInicioCargo(), admC.getFechaFinCargo()));
            Correo.enviarMail(admC.getCorreo(),"Su Usuario y Contrasena como Adm. de la Ciudadela", "Usuario: "+usuario+", Contrasena: "+contrasena);
        }
        else if(user instanceof Residente){
            Residente res= (Residente)user;
            usuario=res.getCedula();
            contrasena=res.getNombre().substring(0, 3)+"res";
            usuarios.add(new Residente(usuario, contrasena, res.getNombre(),res.getCedula(),res.getCorreo(),res.getPhone(),res.getPin(),res.getCasa()));
            Correo.enviarMail(res.getCorreo(),"Su Usuario y Contrasena como Residente de la Ciudadela", "Usuario: "+usuario+", Contrasena: "+contrasena);
        }
    }
    
    
    public Ciudadela referenciarCiudadela(AdminCiudadela admC){ //Me retorna la ciudadela que esta a cargo de un administrador
        for(Ciudadela c: ciudadelas){
            if((c.getAdminCiudadela().getCedula()).equals(admC.getCedula())){
                return c;
            }
        }return null;
    }
    
        
    public static Ciudadela obtenerCiudadela(Residente res){  //Me devuelve la ciudadela a la que pertenece un residente
        for(Ciudadela c: ciudadelas){
            for(Residente r: c.getResidentes()){
                if(r.getCedula().equals(res.getCedula())){
                    return c;
                }
            }
        }return null;
    }
    

    public static Residente buscarResidente(String cedula){  //Me busca el residente por cedula para el reporte de visitas
        for(Usuario u:usuarios){
            if(u instanceof Residente){
                Residente r=(Residente)u;
                if(r.getCedula().equals(cedula) )
                    return r;
            }
        }return null;
    }
    
    
    public static void registrarCasas(Ciudadela c,int numCasas){  //Me pregunta cuantas casas tendra la ciudadela y se las crea
        boolean estado=false;
        while(estado==false){
            if(numCasas<=10){
                for(int i=1; i<=numCasas;i++){
                    c.agregarCasa(new Casa("Mz 1","Villa "+Integer.toString(i),true));
                }
                estado=true;
            }else if(numCasas>10 && numCasas<=20){
                int num=numCasas-10;
                for(int i=1;i<=10;i++){
                    c.agregarCasa(new Casa("Mz 1","Villa "+Integer.toString(i),true));
                }
                for(int i=1; i<=num;i++){
                    c.agregarCasa(new Casa("Mz 2","Villa "+Integer.toString(i+10),true));
                }
                estado=true;
            }else{System.out.println("No se puede crear mas de 20 casas");  }
        }
    }    
    
    
    public static Residente referenciarDuenioVehiculo(String matricula){  //Metodo que devuelve el residente duenio de un vehiculo.
        for(Usuario u: usuarios){
            if(u instanceof Residente){
                Residente r=(Residente)u;
                for(Vehiculo v:r.getVehiculos()){   
                    if ( (v.getnMatricula() ).equals(matricula)){
                        return r;
                    }
                }
            }
        }
        return null;
    }
    

    public static Residente obtenerResidente(String cedula_codigo){  //Me retorna el Residente que creo al visitante pasado por parametro
        for(Usuario u: usuarios){
            if(u instanceof Residente){
                Residente r=(Residente)u;
                for(Visitante visit: r.getVisitantes()){
                    if(cedula_codigo.length()==10){
                        if(visit.getCedula().equals(cedula_codigo))
                            return r;
                    }
                    else if(cedula_codigo.length()==8){
                        if(visit.getPin().equals(cedula_codigo))
                            return r;
                    }  
                }
            }
        }return null;
    }
    
    
    public static Visitante obtenerVisitante(String cedula_codigo){  //Me retorna un visitante dado la cedula
        for(Usuario u: usuarios){
            if(u instanceof Residente){
                Residente r=(Residente)u;
                for(Visitante v: r.getVisitantes()){
                    if(cedula_codigo.length()==10){
                        if(v.getCedula().equals(cedula_codigo))
                            return v;
                    }
                    else if(cedula_codigo.length()==8){
                        if(v.getPin().equals(cedula_codigo))
                            return v;  
                    }
                }
            }
        }return null;
    }
    

    public static boolean verificarVisitante(String cedula_codigo){  //Me retorna un boolean si existe un visitante dado la cedula o codigo
        for(Usuario u: usuarios){
            if(u instanceof Residente){
                Residente r=(Residente)u;
                for(Visitante v: r.getVisitantes()){
                    if(cedula_codigo.length()==10){
                        if(v.getCedula().equals(cedula_codigo))
                            return true;
                    }
                    else if(cedula_codigo.length()==8){
                        if(v.getPin().equals(cedula_codigo))
                            return true;  
                    }
                }
            }
        }return false;
    }
    
  
    public static void llenarArchivoUsos(){
        reporteUsos.clear();
        for(Ciudadela c: ciudadelas){
            double promedio_Visitantes=c.getTiempo_Visitante()/c.getNumVisitante();
            double promedio_Residentes=c.getTiempo_Residente()/c.getNumResidente();
            Sistema.llenarListaReporteUsos(new ReporteDeUsos(c.getNombre(),c.getRazonSocial(),c.getNumVisitante(),promedio_Visitantes , c.getNumResidente(), promedio_Residentes));
        }
    }
    
    
    public static boolean validarCedula(String dato){  //Metodo que valida que un telefono y cedula tenga 10 digitos y empiece con cero
        boolean op=false;
        for(Usuario u: usuarios){
            if(dato.startsWith("0") && dato.length() == 10 ){
                if(u instanceof Residente  ){
                    Residente r=(Residente)u;
                    if(r.getCedula().equals(dato)){
                        System.out.println("ESTA CEDULA YA ESTA EN USO");
                        return false;}                
                }else if( u instanceof AdminCiudadela){
                    AdminCiudadela ad=(AdminCiudadela)u;
                    if(ad.getCedula().equals(dato )){
                        System.out.println("ESTA CEDULA YA ESTA EN USO");
                        return false;   }
                }else if(u instanceof Visitante){
                    Visitante v=(Visitante)u;
                    if(v.getCedula().equals(dato )){
                        System.out.println("ESTA CEDULA YA ESTA EN USO");
                        return false; }
                }
            }else return false;
        }if(dato.startsWith("0") && dato.length() == 10) 
            op= true;
        return op;
    }
    
    
    public static boolean validar(String dato){  //Metodo que valida que un telefono y cedula tenga 10 digitos y empiece con cero
        if(dato.startsWith("0") && dato.length() == 10 ){
            return true;
        }return false;
    }

    
    public static Ciudadela xCiudadela(String nombre){
        for(Ciudadela c: ciudadelas){
            if(c.getNombre().equals(nombre)){
                return c;
            }
        }return null;
    }
    
    
    public static Date ParseFecha(String fecha){  //Metodo que convierte un String a Fecha Date
        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
        Date fechaDate = null;
        try {
            fechaDate = formato.parse(fecha);
        } 
        catch (ParseException ex) 
        {
            System.out.println(ex);
        }
        return fechaDate;
    }

    
    public static ArrayList<ReporteDeUsos> getReporteUsos() {
        return reporteUsos;
    }

    
    public static ArrayList<Ciudadela> getCiudadelas() {
        return ciudadelas;
    }
    
    
    
    public void recorreArregloU(){  //Recorre el ArrayList usuarios para hacer pruebas
        for(int i=0; i<usuarios.size();i++){
            System.out.println(usuarios.get(i)) ;
        }
    }
    
    
    public void recorreArregloC(){  //Recorre el ArrayList ciudadelas para hacer pruebas
        for(int i=0; i<ciudadelas.size();i++){
            System.out.println(ciudadelas.get(i)) ;
        }
    }
    
    public static void recorreArregloC(Ciudadela c){  //Recorre el ArrayList casas para hacer pruebas
        for(int i=0; i<c.getCasas().size();i++){
            System.out.println(c.getCasas().get(i)) ;
        }
    }    
    
}
