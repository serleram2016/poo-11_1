/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package INTERFAZ;

import Sistema.Sistema;
import Bienes.*;
import Correo.Correo;
import Reportes.ReporteDeUsos;
import Reportes.ReporteVisitas;
import Usuarios.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

/**
 *
 * @author GaryBarzola
 */
public class Simulacion {
    private Ciudadela ciudadela;
    private Residente residente;
    private Visitante visitante;
    
    DateFormat formatoFecha;
    Date fechaActual;  //Para saber la fecha exacta en el momento que desee.
    
    Scanner sc;

    
    
    public Simulacion(){
        formatoFecha = new SimpleDateFormat("dd/MM/yyyy");
        sc= new Scanner(System.in);
    }
    
    public void menuSimular(){  //Menu simular que simula el ingreso a una Ciudadela
        String continuar="si";
        do{
            System.out.println("Ingrese una Ciudadela existente: ");
            String nombre= sc.nextLine();
            ciudadela= Sistema.xCiudadela(nombre);
        }while(ciudadela==null);
        
        do{
            System.out.println("1. PUNTO DE ACCESO PARA RESIDENTES.");
            System.out.println("2. PUNTO DE ACCESO PARA VISITANTES.");
            System.out.println("3. SALIR AL MENU PRINCIPAL.");
            System.out.println("Ingrese una opcion: ");
            String op=sc.nextLine();
            
            switch(op){
                case "1":
                    menuIngresoResidente();
                    System.out.println();
                    break;
                    
                case "2":
                    menuIngresoVisitante();
                    System.out.println();
                    break;
                    
                case "3":
                    continuar="";
                    System.out.println();
                    break;
                    
                default:
                    System.out.println("Opcion incorrecta.\n");
            }   
        }while(continuar.equals("si"));  
    }
    
    
    public void menuIngresoResidente(){  // Menu de Ingreso para residentes
        String continuar="si";
        do{ //ciudadela=null; residente=null;
            System.out.println("1. INGRESAR CON VEHICULO.");
            System.out.println("2. INGRESAR COMO PEATON.");
            System.out.println("3. VOLVER AL MENU DE SIMULACION.");
            System.out.println("Ingrese opcion: ");
            String op= sc.nextLine();
            switch(op){    
                case "1":
                    ingresarConVehiculo();
                    System.out.println();
                    break;
  
                case "2":
                    ingresarComoPeaton();
                    System.out.println();
                    break;
                    
                case "3":
                    continuar="";  
                    System.out.println();
                    break;
                
                default:
                    System.out.println("Opcion incorrecta.\n");
                }
            }while(continuar.equals("si"));
    }
    
    
    public void ingresarConVehiculo(){ //METODO PARA INGRESO EN VEHICULO DE RESIDENTES.
        double t1=System.currentTimeMillis()/1000;
        boolean op;
        String matricula;
        int contador=0;
        do{
            System.out.println("Ingrese la matricula correcta del vehiculo(AAA-000): ");
            matricula= sc.nextLine();
            op=ciudadela.buscarVehiculo(matricula);
            contador++;
        }while(op==false && contador<3);
        if(op==false){
            System.out.println(".........INGRESO DENEGADO........."); 
        }else{
            fechaActual=new Date();
            System.out.println(".........INGRESO EXITOSO.........");
            residente=Sistema.referenciarDuenioVehiculo(matricula);
            
            //ciudadela=Sistema.obtenerCiudadela(residente);
            ciudadela.agregarListaReporteVisitas(new ReporteVisitas(formatoFecha.format(fechaActual),"Residente",residente.getNombre(), residente.getCasa().getMz(), residente.getCasa().getVilla(), matricula));
            ciudadela.contarResidente();
            double t2=System.currentTimeMillis()/1000;
            ciudadela.registrarTiempoResidente(t2-t1);
            

        }
    }
    
    
    public void ingresarComoPeaton(){  //METODO QUE ME VERIFICA EL INGRESO COMO PEATON DE UN RESIDENTE
        double t1=System.currentTimeMillis()/1000;
        boolean op;
        String cedula;
        String pin;
        int contador=0;
        do{
            System.out.println("INGRESE NUMERO DE CEDULA: ");
            cedula = sc.nextLine();
            op=Sistema.validar(cedula);
            while (op==false){
                System.out.println("Ingrese un numero de cedula valido: ");
                cedula = sc.nextLine();
                op=Sistema.validar(cedula);
            }
            System.out.println("INGRESE EL PIN DE ACCESO (4 DIGITOS)");
            pin = sc.nextLine();
            op=ciudadela.ingresoPeatonResidente(cedula, pin);
            contador++;
        }while (op==false && contador<3);
        if(op==false){
            System.out.println(".........INGRESO DENEGADO........."); 
        }else{
            fechaActual=new Date();
            System.out.println(".........INGRESO EXITOSO.........");
            residente=Sistema.buscarResidente(cedula);  
            
            //ciudadela=Sistema.obtenerCiudadela(residente);
            ciudadela.agregarListaReporteVisitas(new ReporteVisitas(formatoFecha.format(fechaActual),"Residente",residente.getNombre(),residente.getCasa().getMz(),residente.getCasa().getVilla(), "Peaton"));
            ciudadela.contarResidente();
            double t2=System.currentTimeMillis()/1000;
            ciudadela.registrarTiempoResidente(t2-t1);
        }
    }
    
    
    public void menuIngresoVisitante(){  //Menu de ingreso para visitantes
        String continuar="si";
        do{ //ciudadela=null; visitante=null; residente=null;
            System.out.println("1. TIENE CODIGO DE ACCESO.");
            System.out.println("2. NO TIENE CODIGO DE ACCESO.");
            System.out.println("3. VOLVER AL MENU DE SIMULACION.");
            System.out.println("Ingrese una opcion: ");
            String op= sc.nextLine();
            
            switch(op){
                case "1":
                    tieneCodigoAcceso();
                    System.out.println();
                    break;
                    
                case "2":
                    noTieneCodigoAcceso();
                    System.out.println();
                    break;
                    
                case "3":
                    continuar="";
                    System.out.println();
                    break;
                
                default:
                    System.out.println("Opcion incorrecta.\n");
                }

            }while(continuar.equals("si"));  
    }


    public void tieneCodigoAcceso(){  //Verifica si el visitante tiene un codigo valida y se lo pide hasta 3 veces si no lo es.
        double t1=System.currentTimeMillis()/1000;
        String codigo;
        boolean estado;
        System.out.println("INGRESE EL CODIGO DE ACCESO (8 CARACTERES): ");
        codigo= sc.nextLine();
        estado=ciudadela.validarCodigo(codigo);
        int contador=0;
        while(estado == false && contador<3){
            System.out.println("......Tiene " + (3-contador) + " intentos.....\nINGRESE de nuevo el codigo: ");
            codigo= sc.nextLine();
            estado=ciudadela.validarCodigo(codigo);
            contador++;
        }
        visitante=Sistema.obtenerVisitante(codigo);              
        if (estado && visitante.getEstado()){
            fechaActual=new Date();
            System.out.println(".........INGRESO EXITOSO.........");
            residente=Sistema.obtenerResidente(codigo);
            visitante.setEstado(false);
            
            ciudadela=Sistema.obtenerCiudadela(residente);
            ciudadela.agregarListaReporteVisitas(new ReporteVisitas(formatoFecha.format(fechaActual),"Visitante",visitante.getNombre(),residente.getCasa().getMz(), residente.getCasa().getVilla(), "Peaton" ));            
            ciudadela.contarVisitante();
            double t2=System.currentTimeMillis()/1000;
            ciudadela.registrarTiempoVisitante(t2-t1);
        }
        else { 
            System.out.println("INGRESO INCORRECTO"); 
        }
    }
    
    
    public void noTieneCodigoAcceso(){  //Si un visitante no tiene codigo puede registrarse en el momento siempre y cuando el residente
                                        // Al que visita exista en el sistema, luego se le genera el codigo y tiene que ir al apartado "tiene codigo acceso" para ingresar
        boolean op;
        System.out.println("Ingrese su nombre como VISITANTE: ");
        String nombre = sc.nextLine();
                    
        String cedula;
        System.out.println("Ingrese su cedula como VISITANTE: ");
        cedula = sc.nextLine();
        op=Sistema.validarCedula(cedula);
        while (op==false){
            System.out.println("Ingrese un numero de cedula valido: ");
            cedula = sc.nextLine();
            op=Sistema.validarCedula(cedula);
        }
        System.out.println("Buscando si existe en el sistema...");
        residente=ciudadela.obtenerResidente(cedula);        
        String correoVisitante="";
        if(residente==null){
            String nombreResidente;
            String villa;
            String mz;
            System.out.println("Usted no esta registrado como visitante en el sistema");
            System.out.println("Ingrese su correo: ");
            correoVisitante=sc.nextLine();
            op=Correo.validarCorreo(correoVisitante); 
            while (op == false){
                System.out.println("Ingrese un correo GMAIL valido: ");
                correoVisitante=sc.nextLine();
                op=Correo.validarCorreo(correoVisitante);   
            }

            System.out.println("INGRESE el nombre del residente al que visita: ");
            nombreResidente = sc.nextLine();
            System.out.println("INGRESE la mz del residente: ");
            mz= sc.nextLine();
            System.out.println("INGRESE la villa del residente: ");
            villa = sc.nextLine();
            int contador=0;
            residente=ciudadela.buscarResidenteParaVisitante(nombreResidente,"Mz " +mz,"Villa " +villa);
            while(residente==null && contador<3){
                System.out.println(".........Residente no existente.........");
                System.out.println("INGRESE el nombre del residente al que visita: ");
                nombreResidente = sc.nextLine();
                System.out.println("INGRESE la mz del residente: ");
                mz=sc.nextLine();
                System.out.println("INGRESE la villa del residente: ");
                villa=sc.nextLine();

                residente=ciudadela.buscarResidenteParaVisitante(nombreResidente,"Mz " +mz,"Villa " +villa);
                contador++;
            }

            if(residente!=null){
                System.out.println(".........Residente Encontrado.........");
                String cod=residente.generarCodigo();

                fechaActual=new Date();
                Visitante vis = new Visitante(nombre,correoVisitante, cedula,cod,fechaActual , fechaActual,true );
                residente.agregarVisitante(vis);
                System.out.println("Enviando correo...");
                Correo.enviarMail(correoVisitante, "Codigo de ingreso a la ciudadela", cod);   
            }
            else{
                System.out.println(".........Residente no encontrado.........");
            }  
        }else if(residente!=null && Sistema.obtenerVisitante(cedula).getEstado()){
            System.out.println("Visitante encontrado.\nEnviando correo...");
            String correoVisitante_2=residente.obtenerCorreoVisitante(cedula);
            if(correoVisitante_2.substring(correoVisitante_2.length()-10, correoVisitante_2.length()).equals("@gmail.com")){
                correoVisitante=correoVisitante_2;
                
            }
            String cod=residente.generarCodigo();
            
            if(Sistema.verificarVisitante(cedula)){
                visitante=Sistema.obtenerVisitante(cedula);
                visitante.setPin(cod);
            }else{
                fechaActual=new Date();
                residente.agregarVisitante(new Visitante(nombre,cedula,correoVisitante,cod,fechaActual,fechaActual,true));
            }     
            Correo.enviarMail(correoVisitante, "Codigo de ingreso a la ciudadela", cod);
        }else if(Sistema.obtenerVisitante(cedula).getEstado()==false){
            System.out.println("NO SE PUEDE GENERAR UN CODIGO NUEVO A UN VISITANTE QUE YA HA INGRESADO A LA CIUDADELA.");
        }
    }
    
    
     
    public void inicializarSistema(){ //Crea una ciudadela con 5 casas, dos residentes y un administrador
        Date fecha= new Date();
        AdminCiudadela admC= new AdminCiudadela(null,null,"Carlos","sistemapoo11@gmail.com","0987654333",fecha,fecha);
        
        Sistema.crearUsuario(admC); // User: 0987654333 , Pass: Caradm
        
        Ciudadela ciudade= new Ciudadela("Bellavista","Razon1","Ruc1","Ubicacion1",admC);
        
        Sistema.llenarListaCiudadelas(ciudade);
        
        
        Casa c1= new Casa("Mz 1","Villa 1",true);
        Casa c2=new Casa("Mz 1","Villa 2",true);
        ciudade.agregarCasa(c1);
        ciudade.agregarCasa(c2);
        ciudade.agregarCasa(new Casa("Mz 1","Villa 3",true));
        ciudade.agregarCasa(new Casa("Mz 1","Villa 4",true));
        ciudade.agregarCasa(new Casa("Mz 1","Villa 5",true));
        
        Residente r1= new Residente(null,null,"Pepe","0987654300","sistemapoo11@gmail.com","0988909890","1234",c1);
        Residente r2= new Residente(null,null,"Nacho","0912345678","sistemapoo11@gmail.com","0987676467","1234",c2);
        c1.setDisponibilidad(false);
        c2.setDisponibilidad(false);
        
        Sistema.crearUsuario(r1); // user: 0987654300 , pass: Pepres
        Sistema.crearUsuario(r2); // user: 0912345678 , pass: Nacres

        ciudade.agregarResidente(r1);
        ciudade.agregarResidente(r2);
        
    }
    
}
