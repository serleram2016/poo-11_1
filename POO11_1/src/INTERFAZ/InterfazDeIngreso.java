/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package INTERFAZ;

import Sistema.Sistema;
import Reportes.ReporteVisitas;
import Bienes.*;
import Correo.Correo;
import Reportes.ReporteDeUsos;
import Usuarios.*;
import java.util.*;



public class InterfazDeIngreso {
    private Scanner sc;
    private AdminCiudadela admiCiudadela; //el admin de ciudadel regisrado en la sesion
    private Ciudadela ciudadela; //Ciudadela en la que se esta trabajando
    private AdminSistema admiSistema; //el admin del sistema de la session 
    private Residente residente;  //el residentede la session  
    private Sistema s;
    private Simulacion simulacion;
    Date fechaActual;

    
    public InterfazDeIngreso() {  //Constructor, inicializo los objetos
        sc = new Scanner(System.in);
        s = new Sistema();
        simulacion = new Simulacion();

    }
    
    public static void main( String[] arg){
        InterfazDeIngreso cid = new InterfazDeIngreso();
        cid.iniciar();
    }
        
    
    private void iniciar(){  //Menu principal del sistema 
            String continuar = "si";
            int contador=0;
            if(contador==0){
                System.out.println(".......Creando la ciudadela Bellavista con 5 casas, 2 residentes y 1 administrador......\n"
                +"Espere a que se terminen de enviar los 3 correos con usuarios respectivos...");
                simulacion.inicializarSistema();
                contador++;
            }
            do{
                mostrarOpciones();
                String op = sc.nextLine();
                switch(op){
                    case "1":
                        String estado=validarUsuario();
                        if(!estado.equals("")){
                            if (estado.equals("superadmin")){
                                menuAdminSistema();
                                
                            }
                            else if (estado.equals("adminciud")){
                                menuAdminCiudadela();
                               
                            }
                            else if (estado.equals("residente")){
                                menuResidente();
                                
                            }
                        }
                        break;

                    case "2":
                        System.out.println("\n....... SIMUNLANDO INGRESO.........");
                        simulacion.menuSimular();
                        break;

                    case "3":
                        do{
                            System.out.println("Desea realizar otra opcion en el Menu Principal(si/no): ");
                            continuar=sc.nextLine();
                        }while(!continuar.equalsIgnoreCase("no") && !continuar.equalsIgnoreCase("si"));
                        break;
                        
                    default:
                        System.out.println("Opcion incorrecta");
                        System.out.println();
                }
                if(continuar.equals("no")) System.out.println("GRACIAS POR LA VISITA AL SISTEMA");
            }
        while(continuar.equals("si"));
    }
    
    
    public void mostrarOpciones(){  //Muestra las opciones que ven todos los usuarios.
        System.out.println("       BIENVENIDO AL SISTEMA DE CIUDADELA");
        System.out.println("1.INICIAR SESION.");
        System.out.println("2.SIMULAR INGRESO A CIUDADELA.");
        System.out.println("3.SALIR.");
        System.out.println("Ingrese una opcion: ");
    }
    
  
    public String  validarUsuario(){  //Valida user y pass de un usuario y me retorna si es admin o residente
        int contador=0;
        do{ ciudadela=null; //Limpio el dato
            System.out.println(".............. Tiene "+(3-contador)+" intentos ..............");
            System.out.println("Ingrese usuario: ");
            String user=sc.nextLine();
            System.out.println("Ingrese contrasena: ");
            String pass=sc.nextLine();   
            Usuario u=s.autenticacionUsuario(user, pass);
            if (u instanceof AdminSistema){
                admiSistema = (AdminSistema)u;
                System.out.println("...............USTED HA ENTRADO COMO ADMINISTRADOR DE SISTEMA.............");
                return "superadmin";
            }
            else if (u instanceof AdminCiudadela){
                admiCiudadela = (AdminCiudadela)u;
                ciudadela=s.referenciarCiudadela(admiCiudadela);  //Al ingresar el como admin de ciuadadela referencia la ciudadela que el tiene a cargo.
                System.out.println("...............USTED HA ENTRADO COMO ADMINISTRADOR DE CIUDADELA.............");
                System.out.println("Ciudadela Encargada: "+ciudadela.getNombre());
                return "adminciud";
            }
            else if(u instanceof Residente){
                residente = (Residente)u;
                ciudadela=Sistema.obtenerCiudadela(residente);
                System.out.println("...............USTED HA ENTRADO COMO RESIDENTE SR./Sra. "+residente.getNombre()+".............");
                System.out.println("Ciudadela en la que es residente: "+ciudadela.getNombre());
                return "residente";
            }       
            else{
                contador++;
            }
        }while(contador<=2);
        return "";
    }
    
        
    public void menuAdminSistema(){  //Muestra lo que administrador del sistema puede hacer 
        String continuar="si";
        do{
            System.out.println("1. CREAR CIUDADELA.");
            System.out.println("2. GENERAR REPORTE DE USO DEL SISTEMA.");
            System.out.println("3. SALIR.");
            System.out.println("Ingrese opcion: ");
            String op=sc.nextLine();
            
            switch(op){
                case "1":
                    registrarCiudadela();
                    System.out.println();
                    break;
                    
                case "2":
                    Sistema.llenarArchivoUsos();
                    ReporteDeUsos.crearCSV(Sistema.getReporteUsos());
                    System.out.println("Reporte Generado.\n");
                    break;
                    
                case "3":
                    continuar="";
                    System.out.println();
                    break;
                default:
                    System.out.println("Opcion incorrecta.");
                    System.out.println();
            }   
        }while(continuar.equals("si"));  
    }
    
         
    public void registrarCiudadela(){  // Registra ciudadelas y las agrega al ArrayList de Ciudadelas
        System.out.println("Ingrese nombre de ciudadela: ");
        String nombre=sc.nextLine();
        System.out.println("Ingrese razon social: ");
        String razon=sc.nextLine();
        System.out.println("Ingrese ruc: ");
        String ruc=sc.nextLine();
        System.out.println("Ingrese ubicacion: ");
        String ubicacion=sc.nextLine();
        AdminCiudadela ad=registrarAdminCiudadela();
        ciudadela= new Ciudadela(nombre,razon,ruc,ubicacion,ad);
        System.out.println("\n....UNA VEZ CREADAS LAS CASAS NO SE PUEDEN CREAR MAS....\n"
                +".........SOLO SE PUEDEN CREAR MAXIMO 2 MANZANAS.........\n"
                +".................Y 10 CASAS POR MANZANA.................");
        System.out.println("Cuantas casas desea crear en la ciudadela "+ciudadela.getNombre()+": ");
        int numCasas=sc.nextInt();
        Sistema.registrarCasas(ciudadela, numCasas);
        System.out.println("Casas creadas.");
        admiSistema.registrarCiudadela(ciudadela);
        sc.nextLine(); //Limpio scanner para nuevo ingreso de datos.
    }
      
    
    public AdminCiudadela registrarAdminCiudadela(){  //Registra un administrador de ciudadelas y lo retorna
        int letras;
        String nombre;
        do{
            System.out.println("Ingrese nombre del admin de la Ciudadela: ");
            nombre=sc.nextLine();
            letras=nombre.length();
        }while(letras<3);
        
        String correo="";
        System.out.println("INGRESE el correo: ");
        correo=sc.nextLine();
        
        boolean op=false;
        op=Correo.validarCorreo(correo); 
        while (op == false){
            System.out.println("Ingrese un correo GMAIL CORRECTO: ");
            op=Correo.validarCorreo(correo=sc.nextLine()); 
        }
        String cedula="";
        System.out.println("Ingrese cedula del admin de ciudadela: ");
        cedula = sc.nextLine();
        op=Sistema.validarCedula(cedula);
        while (op==false){
            System.out.println("Ingrese un numero de cedula valido: ");
            op=Sistema.validarCedula(cedula=sc.nextLine());
        }
        fechaActual=new Date();
        System.out.println("Fecha de inicio del cargo(Hoy): "+fechaActual);
        //Date fechaInicio=Sistema.ParseFecha(sc.nextLine());
        System.out.println("Ingrese fecha fin del cargo(dd/MM/yyyy): ");
        Date fechaFin= Sistema.ParseFecha(sc.nextLine());
        Usuario adm= new AdminCiudadela(null, null, nombre, correo, cedula,fechaActual, fechaFin);
        System.out.println("ENVIANDO CORREO...");
        Sistema.crearUsuario(adm);
        return (AdminCiudadela)adm;
    }
   
    
    public void menuAdminCiudadela(){  //MUESTRA LO QUE PUEDE HACER EL ADMINISTRADOR DE CIUDADELA
        String continuar="si";
        do{
            System.out.println("1. REGISTRAR RESIDENTES.");
            System.out.println("2. GENERAR REPORTE DE VISITAS.");
            System.out.println("3. SALIR.");
            System.out.println("Ingrese opcion: ");
            String op=sc.nextLine();
            switch(op){
            case "1":
                registrarResidente();
                System.out.println();
                break;
                
            case "2":
                ReporteVisitas.crearCSV(ciudadela.getListaVisitas(),admiCiudadela.getNombre());
                System.out.println("Reporte Generado.\n");
                break;
                
            case "3":
                continuar="";
                System.out.println();
                break;

            default:
                System.out.println("Opcion incorrecta.");
                System.out.println();
            }
        }while(continuar.equals("si"));
    }
    
    
    public void registrarResidente(){  //Metodo que regitra residentes
        boolean op=false;
        int letras;
        String nombre;
        do{
            System.out.println("Ingrese el nombre del RESIDENTE: ");
            nombre=sc.nextLine();
            letras=nombre.length();
        }while(letras<3);
        
        String cedula="";
        System.out.println("Ingrese la cedula del RESIDENTE");
        cedula = sc.nextLine();
        op=Sistema.validarCedula(cedula);
        while (op==false){
            System.out.println("Ingrese un numero de cedula valido: ");
            op=Sistema.validarCedula(cedula=sc.nextLine());
        }
        
        String correo="";
        System.out.println("INGRESE el correo: ");
        correo=sc.nextLine();
        op=Correo.validarCorreo(correo); 
        while (op == false){
            System.out.println("Ingrese un correo GMAIL valido: ");
            op=Correo.validarCorreo(correo=sc.nextLine()); 
        }

        String telefono="";
        System.out.println("Ingrese el telefono del residente: ");
        telefono=sc.nextLine();
        op=Sistema.validar(telefono);
        while (op==false){
            System.out.println("El Telefono debe empezar por 0 y tener 10 digitos \nIngrese el telefono valido del residente: ");
            op=Sistema.validar(telefono=sc.nextLine());
        }
        
        System.out.println("Casas creadas en la ciudadela "+ciudadela.getNombre()+": ");
        Sistema.recorreArregloC(ciudadela);  //Me muestra todas las casas de la ciudadela y su disponibilidad
        boolean estado;
        String villa;
        String mz;
        Casa c;
        do{
            System.out.println("....ELIGA UNA CASA DISPONIBLE(TRUE).... ");
            System.out.println("Ingrese la Manzana en la que se agregara al residente: ");
            mz=sc.nextLine();
            System.out.println("Ingrese la Villa en la que se agregara el residente: ");
            villa=sc.nextLine();
            c=ciudadela.buscarCasa("Mz "+mz,"Villa "+villa);
            if(c==null){
                estado=false;
            }else{
                estado=c.getDisponibilidad(); 
            }   
        }while(estado==false);
        c.setDisponibilidad(false);
        //ciudadela.recorreArregloC();  ////Me muestra todas las casas de la ciudadela y su disponibilidad
        String pin1 = "1234" ;   //Por defecto, para que luego el residente lo cambie y ponga el que el requiera.
        Usuario re=new Residente(null,null,nombre,cedula,correo,telefono,pin1, c);
        ciudadela.agregarResidente((Residente)re);
        System.out.println("ENVIANDO CORREO...");
        Sistema.crearUsuario(re);  //Le crea un usuario y se lo manda al correo
    }
    

    public void menuResidente(){  //Metodo que muestra lo que puede hacer el residente
        String continuar="si";
        do{
            System.out.println("1. REGISTRAR VEHICULO. ");
            System.out.println("2. REGISTRAR VISITANTES. ");
            System.out.println("3. LISTADO DE VISITANTES.");
            System.out.println("4. ELIMINAR UN VISITANTE.");
            System.out.println("5. CAMBIAR PIN.");
            System.out.println("6. SALIR.");
            System.out.println("Ingrese opcion: ");
            String op=sc.nextLine();
            
            switch(op){
                case "1":
                    int contador=0;
                    String matricula;
                    System.out.println("Nombre del propietario: ");
                    String nombre=sc.nextLine();
                    do{
                        System.out.println("Ingrese matricula(AAA-000): ");
                        matricula=sc.nextLine(); 
                        contador++;
                    }while(matricula.length()!=7 && contador<=2);
                    Vehiculo v= new Vehiculo(matricula, nombre);
                    residente.agregarVehiculo(v);
                    System.out.println("Vehiculo registrado Sr./Sra. "+residente.getNombre()+".");
                    System.out.println();
                    break;

                case "2":
                    registrarVisitante();
                    System.out.println();
                    break;
                    
                case "3":
                    System.out.println("Los visitantes registrados por el residente "+residente.getNombre()+" son: \n");
                    residente.recorrerVisitantes();
                    System.out.println();
                    break;
                
                case "4":
                    boolean opp;
                    String cedula;
                    int cont=0;
                    System.out.println("SOLO SE PUEDEN ELIMINAR VISITANTES QUE NO HAYAN INGRESADO A LA CIUDADELA(ESTADO=TRUE)");
                    System.out.println("Ingrese cedula del VISITANTE a eliminar: ");
                    cedula = sc.nextLine();
                    opp=Sistema.validarCedula(cedula);
                    while (cont<2 && Sistema.obtenerVisitante(cedula)==null){
                        System.out.println("Ingrese el numero de cedula valido del VISITANTE: ");
                        opp=Sistema.validarCedula(cedula=sc.nextLine());
                        cont++;
                    }
                    if(opp && Sistema.obtenerVisitante(cedula)!=null && Sistema.obtenerVisitante(cedula).getEstado()){
                        residente.eliminarVisitante(cedula);
                        System.out.println("Visitante eliminado con exito.\n");
                    }
                    else System.out.println("NO ES POSIBLE ELIMINAR EL VISITANTE.\n"); 
                    break;
                    
                case "5":
                    System.out.println("Escriba el nuevo pin: ");
                    String pinc=sc.nextLine();
                    residente.setPin(pinc);
                    System.out.println("Pin cambiado exitosamente");
                    System.out.println();
                    break;
                    
                case "6":
                    continuar="";
                    System.out.println();
                    break;
                    
                default:
                    System.out.println("Opcion incorrecta.\n");
            }
            
        }while(continuar.equals("si"));
    }
        
        
    public void registrarVisitante(){ 
        boolean op;
        System.out.println("Ingrese nombre del VISITANTE: ");
        String nombre = sc.nextLine();

        String cedula;
        System.out.println("Ingrese cedula del VISITANTE: ");
        cedula = sc.nextLine();
        op=Sistema.validarCedula(cedula);
        while (op==false){
            System.out.println("Ingrese un numero de cedula valido: ");
            op=Sistema.validarCedula(cedula=sc.nextLine());
        }
        
        String correo;
        System.out.println("Ingrese el correo del VISITANTE: ");
        correo=sc.nextLine();
        op=Correo.validarCorreo(correo); 
        while (op == false){
            System.out.println("Ingrese un correo GMAIL CORRECTO: ");
            op=Correo.validarCorreo(correo=sc.nextLine());
        }
        fechaActual=new Date();
        System.out.println("Fecha de creacion(Hoy): "+fechaActual);
        System.out.println("Ingrese fecha de INGRESO (dd/MM/yyyy): ");
        Date Fi=Sistema.ParseFecha(sc.nextLine());
        System.out.println("EL REGISTRO HA FINALIZADO CON EXITO Y EL CODIGO SE ESTA ENVIANDO AL CORREO DEL VISITANTE");
        System.out.println(".............VALIDO DURANTES 12 HORAS ANTES Y DESPUES DE LA FECHA DE INGRESO............");
        String codigo= residente.generarCodigo();
        
        Visitante vis = new Visitante(nombre,correo, cedula,codigo,Fi, fechaActual,true);
        residente.agregarVisitante(vis);
        Correo.enviarMail(correo, "Codigo de visitante para ingreso a la Ciudadela", codigo);
    }
    
}
