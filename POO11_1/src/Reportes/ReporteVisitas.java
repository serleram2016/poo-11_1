/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Reportes;

import com.csvreader.CsvWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;


/**
 *
 * @author GaryBarzola
 */
public class ReporteVisitas {
    private String fecha,tipo,nombre,mz,villa,matricula;

    public ReporteVisitas(String fecha, String tipoVisitante, String nombre, String mz, String villa, String matricula) {
        this.fecha = fecha;
        this.tipo = tipoVisitante;
        this.nombre = nombre;
        this.mz = mz;
        this.villa = villa;
        this.matricula = matricula;
    }
    
    
    
    
    public static void crearCSV(ArrayList<ReporteVisitas> r, String nombre){
        String nomArchivo="Reporte_Visitas_"+nombre+".csv";
        if(new File(nomArchivo).exists()){
            File archivo= new File(nomArchivo);
            archivo.delete();
        }
        try{
            CsvWriter salidaCSV= new CsvWriter(new FileWriter(nomArchivo,true), ',');
            salidaCSV.write("Fecha Ingreso");
            salidaCSV.write("Usuario");
            salidaCSV.write("Nombre");
            salidaCSV.write("Mz Residente");
            salidaCSV.write("Villa Residente");
            salidaCSV.write("Matricula Vehiculo");
            
            salidaCSV.endRecord();
            
            for(ReporteVisitas s: r){
                salidaCSV.write(s.getFecha());
                salidaCSV.write(s.getTipo());
                salidaCSV.write(s.getNombre());
                salidaCSV.write(s.getMz());
                salidaCSV.write(s.getVilla());
                salidaCSV.write(s.getMatricula());
                
                salidaCSV.endRecord();
            }
            salidaCSV.close();
            
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    
    
    
    public String getFecha() {
        return fecha;
    }

    public String getTipo() {
        return tipo;
    }

    public String getNombre() {
        return nombre;
    }

    public String getMz() {
        return mz;
    }

    public String getVilla() {
        return villa;
    }

    public String getMatricula() {
        return matricula;
    }

    
}
