/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Reportes;

import com.csvreader.CsvWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author GaryBarzola
 */
public class ReporteDeUsos {
    private String nombreC,razon;
    private int numVisitantes, numResidentes;
    private double promVisitantes, promResidentes;

    public ReporteDeUsos(String nombreC, String razon, int numVisitantes, double promVisitantes, int numResidentes, double promResidentes) {
        this.nombreC = nombreC;
        this.razon = razon;
        this.numVisitantes = numVisitantes;
        this.numResidentes = numResidentes;
        this.promVisitantes = promVisitantes;
        this.promResidentes = promResidentes;
    }

    
    public static void crearCSV(ArrayList<ReporteDeUsos> r){
        String nomArchivo="Reporte_De_Uso_Del_Sistema.csv";
        if(new File(nomArchivo).exists()){
            File archivo= new File(nomArchivo);
            archivo.delete();
        }
        try{
            CsvWriter salidaCSV= new CsvWriter(new FileWriter(nomArchivo,true), ',');
            salidaCSV.write("Nombre Ciudadela");
            salidaCSV.write("Razon Social");
            salidaCSV.write("Numero de ingresos Visitante");
            salidaCSV.write("Tiempo promedio Visitantes(s)");
            salidaCSV.write("Numero de ingresos Residente");
            salidaCSV.write("Tiempo promedio Residentes(s)");
            
            salidaCSV.endRecord();
            
            for(ReporteDeUsos s: r){
                salidaCSV.write(s.getNombreC());
                salidaCSV.write(s.getRazon());
                salidaCSV.write(Integer.toString(s.getNumVisitantes()));
                salidaCSV.write(Double.toString(s.getPromVisitantes()));
                salidaCSV.write(Integer.toString(s.getNumResidentes()));
                salidaCSV.write(Double.toString(s.getPromResidentes()));
                
                salidaCSV.endRecord();
            }
            salidaCSV.close();
            
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    
    public String getNombreC() {
        return nombreC;
    }

    public String getRazon() {
        return razon;
    }

    public int getNumVisitantes() {
        return numVisitantes;
    }

    public int getNumResidentes() {
        return numResidentes;
    }

    public double getPromVisitantes() {
        return promVisitantes;
    }

    public double getPromResidentes() {
        return promResidentes;
    }
    
    
}
